package hello;

public class Sum {
    private final Integer total;

    public Sum(Integer value1, Integer value2) {
        this.total = value1 + value2;
    }

    public Integer getTotal() {
        return total;
    }
}
