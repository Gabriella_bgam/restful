package hello;

import com.google.gson.JsonObject;
import org.springframework.web.bind.annotation.*;

@RestController
// Allow incoming connections from cross-origin sources (for example, the frontend)
@CrossOrigin
public class RestfulController {

    // Requests will be made on http://localhost:8080/endpoint
    @RequestMapping(value = "/endpoint", method = RequestMethod.GET, produces = {"text/plain", "application/json"})
    public String doGet(@RequestParam(value="alma", defaultValue = "") String alma,
                          @RequestParam(value="korte", defaultValue = "") String korte,
                          @RequestParam(value="format", defaultValue = "text") String format) {
        String returnValue = "";

        if (!alma.isEmpty() && !korte.isEmpty()) {
            if (format.equals("json")) {
                JsonObject json = new JsonObject();
                json.addProperty("alma", alma);
                json.addProperty("korte", korte);
                returnValue = json.toString();
            }
            else {
                returnValue = "alma: " + alma + ", korte: " + korte;
            }
        }
        else {
            if (format.equals("json")) {
                JsonObject json = new JsonObject();
                json.addProperty("message", "hello world");
                returnValue = json.toString();
            }
            else {
                returnValue = "hello world";
            }
        }

        return returnValue;
    }

    @RequestMapping(value = "/endpoint", method = RequestMethod.POST)
    public String doPost(@RequestParam(value="value1", defaultValue="0") String value1,
                         @RequestParam(value="value2", defaultValue="0") String value2,
                         @RequestParam(value="format", defaultValue="text") String format
                         ) {
        String returnValue = "";
        Integer value1Int = Integer.parseInt(value1);
        Integer value2Int = Integer.parseInt(value2);

        Sum sum = new Sum(value1Int, value2Int);

        if (format.equals("json")) {
            JsonObject json = new JsonObject();
            json.addProperty("sum", sum.getTotal());
            returnValue = json.toString();
        }
        else {
            returnValue = "sum is: " + sum.getTotal().toString();
        }

        return returnValue;
    }
}
